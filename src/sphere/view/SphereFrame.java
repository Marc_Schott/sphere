package sphere.view;

import sphere.model.Sphere;
import sphere.model.SphereBox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by marc on 08.10.16.
 */
public final class SphereFrame implements Observer {
    private static final int REFRESH_RATE = 25;
    private JFrame window;
    private SphereBox box;
    private SphereBoxPanel spherePanel;

    public SphereFrame() {
        window = new JFrame("PhysicsBall in box");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.setPreferredSize(new Dimension(640, 480));
        spherePanel = new SphereBoxPanel(640, 480, Color.BLUE, SphereBox.RADIUS);
        window.setContentPane(spherePanel);
        spherePanel.setVisible(true);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setBackground(Color.WHITE);
        box = new SphereBox(640, 480, this);
    }

    public static void main(String[] args) {
        SphereFrame frame = new SphereFrame();
        frame.display();
    }

    public void update(Observable o, Object obj) {
        double[] newPosition = ((Sphere) o).getPosition();
        double[] newVelocity = ((Sphere) o).getVelocity();
        spherePanel.setSpherePosition((int) Math.round(newPosition[0]), (int)
                Math.round(newPosition[1]));
        if (newVelocity[0] == 0 && newVelocity[1] == 0) {
            spherePanel.setMoving(false);
        } else {
            spherePanel.setMoving(true);
        }
    }

    public void display() {
        window.setVisible(true);
        ActionListener Refresher = (e -> SwingUtilities.invokeLater(() -> {
            window.repaint();
            window.revalidate();
        }));
        Timer timer = new Timer(REFRESH_RATE, Refresher);
        timer.setRepeats(true);
        timer.start();
    }
}
