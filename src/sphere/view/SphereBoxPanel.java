package sphere.view;

import javax.swing.*;
import java.awt.*;

/**
 * Created by marc on 09.10.16.
 */
class SphereBoxPanel extends JComponent {
    private SphereData visibleSphere;
    private Color color;

    SphereBoxPanel(int length, int width, Color colorBall, int radius) {
        setPreferredSize(new Dimension(length, width));
        this.color = colorBall;
        visibleSphere = new SphereData((int) Math.round(Math.random() *
                (length - 2 * radius) + radius), (int) Math.round(Math.random
                () * (width - 2 * radius) + radius), radius);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(color);
        g.fillOval(visibleSphere.getX() - visibleSphere.getRadius(),
                visibleSphere.getY() - visibleSphere.getRadius(),
                visibleSphere.getRadius() * 2, visibleSphere.getRadius() * 2);
        g.setColor(Color.BLACK);
        g.drawString(visibleSphere.isMoving() ? "moving" : "standing", 15, 25);
    }

    void setSpherePosition(int x, int y) {
        visibleSphere.setX(x);
        visibleSphere.setY(y);
    }

    void setMoving (boolean moving) {
        visibleSphere.setMoving(moving);
    }
}
