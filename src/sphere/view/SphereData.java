package sphere.view;

/**
 * Created by marc on 09.10.16.
 */
class SphereData {
    private int x;
    private int y;
    private int radius;
    private boolean moving;

    SphereData(int x, int y, int radius) {
        this.x = Math.round(x);
        this.y = y;
        this.radius = radius;
        this.moving = true;
    }

    int getX() {
        return x;
    }

    void setX(int x) {
        this.x = x;
    }

    int getY() {
        return y;
    }

    void setY(int y) {
        this.y = y;
    }

    int getRadius() {
        return radius;
    }

    void setMoving (boolean moving) {
        this.moving = moving;
    }

    boolean isMoving () {
        return moving;
    }
}
