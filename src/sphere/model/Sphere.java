package sphere.model;

import java.util.Arrays;
import java.util.Observable;

/**
 * Created by marc on 08.10.16.
 */
public class Sphere extends Observable {
    private double[] position;
    private double[] velocity;

    public Sphere(double[] position, double[] velocity) {
        if (position == null || velocity == null || !(position.length == 2)
                || !isValidVelocity(velocity)) {
            throw new IllegalArgumentException("Error! The initial position "
                    + "and velocity of a new sphere must be given as a "
                    + "two-dimensional array, the radius must be positive and "
                    + "the values of the velocity must be within the range of "
                    + -SphereBox.MAX_SPEED + "and " + SphereBox.MAX_SPEED
                    + "!");
        }
        this.position = Arrays.copyOf(position, 2);
        this.velocity = Arrays.copyOf(velocity, 2);
    }

    public synchronized double[] getPosition() {
        return Arrays.copyOf(position, 2);
    }

    public synchronized double[] getVelocity() {
        return Arrays.copyOf(velocity, 2);
    }

    synchronized void randomVelocity() {
        double[] newVelocity = new double[2];
        newVelocity[0] = 2 * (Math.random() - 1);
        newVelocity[1] = 2 * (Math.random() - 1);
        this.velocity = newVelocity;
        setChanged();
        notifyObservers();
    }

    synchronized void move(int movementTime) {
        double angle = Math.atan(Math.abs(velocity[1] / velocity[0]));
        double[] decelaration = new double[]{Math.cos(angle) * SphereBox
                .FRICTION_DECELARATION, Math.sin(angle) * SphereBox
                .FRICTION_DECELARATION};
        for (int i = 0; i < 2; i++) {
            if (velocity[i] > 0) {
                position[i] = Math.floor(-0.5 * decelaration[i]
                        * Math.pow(movementTime, 2) + velocity[i] * movementTime
                        + position[i]);
                if (velocity[i] <= -movementTime * decelaration[i]) {
                    velocity[i] = 0;
                } else {
                    velocity[i] = velocity[i] - movementTime
                            * (-decelaration[i]);
                }
            } else if (velocity[i] < 0) {
                position[i] = Math.ceil(0.5
                        * decelaration[i]
                        * Math.pow(movementTime, 2) + velocity[i] * movementTime
                        + position[i]);
                if (-velocity[i] <= movementTime * (-decelaration[i])) {
                    velocity[i] = 0;
                } else {
                    velocity[i] = velocity[i] - movementTime
                            * decelaration[i];
                }
            }
        }
        setChanged();
        notifyObservers();
    }

    synchronized void collide(MovementDirection collisionDirection) {
        switch (collisionDirection) {
            case HORIZONTAL:
                velocity[0] = (-velocity[0])
                        * (1 - SphereBox.COLLISION_FRICTION_PERCENTAGE);
                velocity[1] = velocity[1] * (1 - SphereBox.COLLISION_FRICTION_PERCENTAGE);
                setChanged();
                notifyObservers();
                break;
            case VERTICAL:
                velocity[1] = (-velocity[1])
                        * (1 - SphereBox.COLLISION_FRICTION_PERCENTAGE);
                velocity[0] = velocity[0]
                        * (1 - SphereBox.COLLISION_FRICTION_PERCENTAGE);
                setChanged();
                notifyObservers();
                break;
            case HORIZONTALANDVERTICAL:
                velocity[0] = (-velocity[0])
                        * (1 - SphereBox.COLLISION_FRICTION_PERCENTAGE);
                velocity[1] = (-velocity[1])
                        * (1 - SphereBox.COLLISION_FRICTION_PERCENTAGE);
                setChanged();
                notifyObservers();
                break;
            default:
                throw new Error();
        }
    }

    private boolean isValidVelocity(double[] velocity) {
        assert velocity != null;
        return velocity.length == 2 && velocity[0] >= -SphereBox.MAX_SPEED
                && velocity[1] >= -SphereBox.MAX_SPEED
                && velocity[0] <= SphereBox.MAX_SPEED
                && velocity[1] <= SphereBox.MAX_SPEED;
    }
}
