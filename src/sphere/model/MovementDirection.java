package sphere.model;

/**
 * Created by marc on 08.10.16.
 */
public enum MovementDirection {

    HORIZONTAL,

    VERTICAL,

    HORIZONTALANDVERTICAL
}
