package sphere.model;

import sphere.view.SphereFrame;

/**
 * Created by marc on 08.10.16.
 */
public class SphereBox {
    public static final int RADIUS = 20;
    static final double FRICTION_DECELARATION = -0.0001;
    static final double COLLISION_FRICTION_PERCENTAGE = 0.05;
    static final int MAX_SPEED = 1;
    private static final int TIMEUNIT = 1;
    private static final int CALCULATION_PAUSE = 10;
    private final int length;
    private final int width;
    private Sphere sphere;

    public SphereBox(int length, int width, SphereFrame window) {
        this.length = length;
        this.width = width;
        double[] position = new double[]{Math.ceil(Math.random() * (length
                - 2 * RADIUS) + 20), Math.ceil(Math.random()
                * (width - 2 * RADIUS) + 20)};
        double[] velocity = new double[]{Math.random(), Math.random()};
        sphere = new Sphere(position, velocity);
        sphere.addObserver(window);
        Thread physicsCalculator = new Thread(this::simulateMovement);
        physicsCalculator.start();
    }

    private boolean[] checkForCollisions(int movementTimeUnits) {
        if (movementTimeUnits <= 0) {
            throw new IllegalArgumentException("There must be a positive "
                    + "amount of time given in order to determine whether the "
                    + "sphere will collide or not!");
        }
        int movementTime = movementTimeUnits * TIMEUNIT;
        double[] velocity = sphere.getVelocity();
        double[] currentPosition = sphere.getPosition();
        double newHorizontalPosition;
        double newVerticalPosition;
        double angle = Math.atan(Math.abs(velocity[1] / velocity[0]));
        double horDecel = Math.cos(angle) * FRICTION_DECELARATION;
        double vertDecel = Math.sin(angle) * FRICTION_DECELARATION;
        if (velocity[0] > 0) {
            newHorizontalPosition = 0.5 * horDecel
                    * Math.pow(movementTime, 2) + velocity[0] * movementTime
                    + currentPosition[0];
        } else {
            newHorizontalPosition = -0.5 * horDecel
                    * Math.pow(movementTime, 2) + velocity[0] * movementTime
                    + currentPosition[0];
        }
        if (velocity[1] > 0) {
            newVerticalPosition = 0.5 * vertDecel
                    * Math.pow(movementTime, 2) + velocity[1] * movementTime
                    + currentPosition[1];
        } else {
            newVerticalPosition = -0.5 * vertDecel
                    * Math.pow(movementTime, 2) + velocity[1] * movementTime
                    + currentPosition[1];
        }
        boolean horizontalCollisionWillHappen = newHorizontalPosition < RADIUS
                || newHorizontalPosition > length - RADIUS;
        boolean verticalCollisionWillHappen = newVerticalPosition < RADIUS
                || newVerticalPosition > width - RADIUS;
        boolean[] collisionsHappening = new boolean[2];
        collisionsHappening[0] = horizontalCollisionWillHappen;
        collisionsHappening[1] = verticalCollisionWillHappen;
        return collisionsHappening;
    }

    private int getTimeToCollision(MovementDirection direction) {
        double positionHor = sphere.getPosition()[0];
        double positionVert = sphere.getPosition()[1];
        double velocityHor = sphere.getVelocity()[0];
        double velocityVert = sphere.getVelocity()[1];
        double angle = Math.atan(Math.abs(velocityVert / velocityHor));
        double horDecel;
        double vertDecel;
        switch (direction) {
            case HORIZONTAL:
                horDecel = Math.cos(angle) * FRICTION_DECELARATION;
                if (velocityHor >= 0) {
                    return (int) Math.floor((-velocityHor + Math.sqrt(Math
                            .pow(velocityHor, 2) - 2 * horDecel *
                            (positionHor - (length - RADIUS)))) / horDecel);
                } else {
                    return (int) Math.ceil((-velocityHor - Math.sqrt(Math.pow
                            (velocityHor, 2) - 2 * (-horDecel) * (positionHor
                            - RADIUS))) / (-horDecel));
                }
            case VERTICAL:
                vertDecel = Math.sin(angle) * FRICTION_DECELARATION;
                if (velocityVert >= 0) {
                    return (int) Math.floor((-velocityVert + Math.sqrt(Math
                            .pow(velocityVert, 2) - 2 * vertDecel *
                            (positionVert - (width - RADIUS)))) / vertDecel);
                } else {
                    return (int) Math.ceil((-velocityVert - Math.sqrt(Math
                            .pow(velocityVert, 2) - 2 * (-vertDecel) *
                            (positionVert - RADIUS))) / (-vertDecel));
                }
            case HORIZONTALANDVERTICAL:
                horDecel = Math.cos(angle) * FRICTION_DECELARATION;
                vertDecel = Math.sin(angle) * FRICTION_DECELARATION;
                if (velocityHor >= 0) {
                    int timeToHorizontalCollision = (int) Math.floor(
                            (-velocityHor + Math.sqrt(Math.pow(velocityHor,
                                    2) - 2 * horDecel * (positionHor -
                                    (length - RADIUS)))) / horDecel);
                    int timeToVerticalCollision;
                    if (velocityVert >= 0) {
                        timeToVerticalCollision = (int) Math.floor(
                                (-velocityVert + Math.sqrt(Math.pow
                                        (velocityVert, 2) - 2 * vertDecel *
                                        (positionVert - (width - RADIUS)))) /
                                        vertDecel);
                    } else {
                        timeToVerticalCollision = (int) Math.ceil(
                                (-velocityVert - Math.sqrt(Math.pow
                                        (velocityVert, 2) - 2 * (-vertDecel)
                                        * (positionVert - RADIUS))) /
                                        (-vertDecel));
                    }
                    return timeToHorizontalCollision <=
                            timeToVerticalCollision ?
                            timeToHorizontalCollision : timeToVerticalCollision;
                } else {
                    int timeToHorizontalCollision = (int) Math.ceil(
                            (-velocityHor - Math.sqrt(Math.pow(velocityHor,
                                    2) - 2 * (-horDecel) * (positionHor -
                                    RADIUS))) / (-horDecel));
                    int timeToVerticalCollision;
                    if (velocityVert >= 0) {
                        timeToVerticalCollision = (int) Math.floor(
                                (-velocityVert + Math.sqrt(Math.pow
                                        (velocityVert, 2) - 2 * vertDecel *
                                        (positionVert - (width - RADIUS)))) /
                                        vertDecel);
                    } else {
                        timeToVerticalCollision = (int) Math.ceil(
                                (-velocityVert - Math.sqrt(Math.pow
                                        (velocityVert, 2) - 2 * (-vertDecel)
                                        * (positionVert - RADIUS))) /
                                        (-vertDecel));
                    }
                    return timeToHorizontalCollision <=
                            timeToVerticalCollision ?
                            timeToHorizontalCollision : timeToVerticalCollision;
                }
            default:
                throw new Error();
        }
    }

    private void executeCollision(MovementDirection direction) {
        int timeToCollision = getTimeToCollision(direction);
        try {
            Thread.sleep((long) timeToCollision);
        } catch (InterruptedException e) {
        }
        sphere.move(timeToCollision);
        sphere.collide(direction);
    }

    private void simulateMovement() {
        while (true) {
            if (sphere.getVelocity()[0] == 0 && sphere.getVelocity()[1] == 0) {
                try {
                    Thread.sleep((long) Math.ceil(Math.random() * 10000));
                } catch (InterruptedException e) {
                }
                sphere.randomVelocity();
            } else {
                boolean[] collisionsAhead = checkForCollisions(
                        SphereBox.CALCULATION_PAUSE);
                if (collisionsAhead[0]) {
                    if (collisionsAhead[1]) {
                        executeCollision(MovementDirection
                                .HORIZONTALANDVERTICAL);
                    } else {
                        executeCollision(MovementDirection.HORIZONTAL);
                    }
                } else if (collisionsAhead[1]) {
                    executeCollision(MovementDirection.VERTICAL);
                } else {
                    try {
                        Thread.sleep(SphereBox.CALCULATION_PAUSE);
                    } catch (InterruptedException e) {
                    }
                    sphere.move(SphereBox.CALCULATION_PAUSE * TIMEUNIT);
                }
            }
        }
    }
}

